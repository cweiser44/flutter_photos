# Photos

A very simple Flutter app that pulls a list of photo albums from [JSON Placeholder](https://jsonplaceholder.typicode.com) and displays the images.

## Screenshots

![]()<img src="https://i.imgur.com/2i7Pwds.png" width="250"/>
![]()<img src="https://i.imgur.com/vOaY5CK.png" width="250"/>

## Views

- List of all albums
- List of all photos in an album
