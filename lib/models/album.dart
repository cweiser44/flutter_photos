class Album {
  final int id;
  //final String userId;
  final String title;

  const Album({this.id, this.title});

  static Album fromJson(Map<String, dynamic> json) {
    return Album(id: json['id'], title: json['title']);
  }

  @override
  String toString() {
    return 'Album(id: $id, title: $title)';
  }

}
