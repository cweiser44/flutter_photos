class Photo {
  final int id;

  final int albumId;

  final String title;

  final String url;

  final String thumbnailUrl;

  Photo({this.id, this.albumId, this.url, this.thumbnailUrl, this.title});

  static Photo fromJson(Map<String, dynamic> json) {
    return Photo(
        id: json['id'],
        title: json['title'],
        url: json['url'],
        thumbnailUrl: json['thumbnailUrl']);
  }

  @override
  String toString() {
    return "Photo(id: $id, albumId: $albumId, title: $title, url: $url, thumbnailUrl: $thumbnailUrl)";
  }

  
}
