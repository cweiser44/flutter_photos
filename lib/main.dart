import 'package:flutter/material.dart';
import 'package:photos/views/all_albums/all_albums.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Photos',
        home: AllAlbumsView());
  }
}

void main() {
  runApp(App());
}
