import 'dart:convert';

import 'package:photos/constants/api_paths.dart';
import 'package:photos/models/photo.dart';
import "package:http/http.dart" as http;

class PhotoApi {

  Future<List<Photo>> getAllPhotosByAlbum(int albumId) async {
    final uri = Uri.https(BASE_URL, "/photos", {"albumId": albumId.toInt().toString()});

    final response = await http.get(uri);

    if (response.statusCode == 200) {
      final parsed = jsonDecode(response.body);
      return parsed.map<Photo>((json) => Photo.fromJson(json)).toList();
    } else {
      throw Exception("Failed to load photos.");
    }
  }
}
