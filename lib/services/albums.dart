import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:photos/constants/api_paths.dart';
import 'package:photos/models/album.dart';

class AlbumApi {

  // make http request and return List<Album>
  Future<List<Album>> getAllAlbums() async {
    final uri = Uri.https(BASE_URL, "/albums");

    final response = await http.get(uri);

    if (response.statusCode == 200) {
      // parse response body into List<Album>
      final parsed = jsonDecode(response.body);
      return parsed.map<Album>((json) => Album.fromJson(json)).toList();
    } else {
      throw Exception('Failed to load albums.');
    }
  }
}
