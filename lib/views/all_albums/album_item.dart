import "package:flutter/material.dart";
import 'package:photos/models/album.dart';
import 'package:photos/views/album/album.dart';

class AlbumItem extends StatelessWidget {
  AlbumItem({@required this.album});
  final Album album;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(album.title),
      leading: Icon(Icons.photo_album),
      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AlbumView(album: album))),
    );
  }
}
