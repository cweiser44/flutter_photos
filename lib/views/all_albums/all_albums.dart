import "package:flutter/material.dart";
import 'package:photos/views/all_albums/album_list.dart';

class AllAlbumsView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Albums"),
      ),
      body: Container(child: AlbumList()),
    );
  }
}
