import "package:flutter/material.dart";
import 'package:photos/models/album.dart';
import 'package:photos/services/albums.dart';
import 'package:photos/views/all_albums/album_item.dart';

class AlbumList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AlbumListState();
  }
}

class _AlbumListState extends State<AlbumList> {
  // controller for infinite loading
  //ScrollController _scrollController = ScrollController();

  // album webservice
  AlbumApi _albumApi = AlbumApi();

  // future to load the initial batch of albums
  Future<List<Album>> _albumsFuture;

  @override
  void initState() {
    super.initState();
    _albumsFuture = _albumApi.getAllAlbums();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return ListView.separated(
            itemBuilder: (c, i) => AlbumItem(album: snapshot.data[i]),
            separatorBuilder: (c, i) => Divider(),
            itemCount: snapshot.data.length,
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
      future: _albumsFuture,
    );
  }
}
