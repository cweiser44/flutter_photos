import 'package:flutter/material.dart';
import 'package:photos/models/album.dart';
import 'package:photos/views/album/photo_list.dart';

class AlbumView extends StatelessWidget {
  AlbumView({@required this.album});

  final Album album;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(album.title)),
      body: PhotoList(albumId: album.id),
    );
  }
}
