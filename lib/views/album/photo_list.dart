import "package:flutter/material.dart";
import 'package:photos/models/photo.dart';
import 'package:photos/services/photos.dart';

class PhotoList extends StatefulWidget {
  PhotoList({@required this.albumId});

  final int albumId;

  @override
  State<StatefulWidget> createState() {
    return _PhotoListState(albumId);
  }
}

class _PhotoListState extends State<PhotoList> {
  Future<List<Photo>> _photosFuture;
  PhotoApi _photoApi = PhotoApi();

  final int albumId;

  _PhotoListState(this.albumId);

  @override
  void initState() {
    super.initState();
    _photosFuture = _photoApi.getAllPhotosByAlbum(albumId);
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return GridView.count(
            crossAxisCount: 3,
            children: List.generate(snapshot.data.length,
                (index) => Image.network(snapshot.data[index].url),
                growable: true),
          );
        } else if (snapshot.hasError) {
          return Center(
            child: Text(snapshot.error.toString()),
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
      future: _photosFuture,
    );
  }
}
